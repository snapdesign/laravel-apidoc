<?php

namespace Snapdesign\Laravel\ApiDoc\Providers;

use Illuminate\Support\ServiceProvider;
use Snapdesign\Laravel\ApiDoc\Console\Commands\ApiDoc;

class ApiDocServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                ApiDoc::class
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $config = __DIR__.'/../../config/apiDoc.php';

        $this->publishes([
            $config => config_path('apiDoc.php'),
        ]);

        $this->mergeConfigFrom(
            $config, 'apiDoc'
        );
    }
}
