<?php

namespace Snapdesign\Laravel\ApiDoc\Models;


interface ValidationRule
{
    /**
     * Get the input name
     *
     * @return string
     */
    public function getInputName();

    /**
     * Return the ules for the input
     *
     * @return []
     */
    public function getRules();
}