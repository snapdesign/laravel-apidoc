<?php

namespace Snapdesign\Laravel\ApiDoc\Models;


interface Route
{
    /**
     * Get the name of the route
     *
     * @return string
     */
    public function getName();

    /**
     * Get the uri of the route
     *
     * @return string
     */
    public function getUri();

    /**
     * Get the group id of the route
     *
     * @return string
     */
    public function getGroupId();

    /**
     * Get the key for sorting the routes
     *
     * @return string
     */
    public function getSortKey();

    /**
     * Get the http-methods of the route
     *
     * @return array
     */
    public function getMethods();

    /**
     * Get the validation rules of the route
     *
     * @return ValidationRule[]
     */
    public function getValidationRules();

    /**
     * Get the Middleware for the route
     *
     * @return []
     */
    public function getMiddleware();
}