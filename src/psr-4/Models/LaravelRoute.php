<?php

namespace Snapdesign\Laravel\ApiDoc\Models;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route as OriginalRoute;
use ReflectionClass;

class LaravelRoute implements Route
{
    /**
     * Loader Config
     *
     * @var array
     */
    protected $config;

    /**
     * Original Route
     *
     * @var LaravelRoute
     */
    protected $originalRoute;

    /**
     * Route name
     *
     * @var string
     */
    protected $name;

    /**
     * is the Route action a closure
     *
     * @var bool
     */
    protected $isClosure;

    /**
     * Validation Rules
     *
     * @var ValidationRule[]
     */
    protected $validationRules;

    /**
     * Class name of the route action
     *
     * @var string
     */
    protected $actionClass = '';

    /**
     * Method name of the route action
     *
     * @var string
     */
    protected $actionMethod = '';

    /**
     * Rest methods
     * Used for sortId
     *
     * @var array
     */
    protected $restMethods = ['index', 'create', 'store', 'show', 'update', 'destroy'];

    /**
     * LaravelRoute constructor.
     *
     * @param OriginalRoute $route
     * @param array $config
     */
    public function __construct(OriginalRoute $route, $config = [])
    {
        $this->originalRoute = $route;
        $this->config = $config;

        $this->name = !empty($route->getName()) ? $route->getName() : $this->getUri();
        $this->isClosure = $route->getActionName() === "Closure";
        if (!$this->isClosure) {
            list($this->actionClass, $this->actionMethod) = explode('@', $route->getActionName());
        }

        $this->validationRules = $this->fetchValidationRules();
    }

    /**
     * Fetch all validation rules of the route
     *
     * @return LaravelValidationRule[]|null
     */
    protected function fetchValidationRules()
    {
        if ($this->isClosure) return null;

        $reflection = new ReflectionClass($this->originalRoute->getController());
        $actionName = $this->originalRoute->getActionName();
        $methodName = substr($actionName, strpos($actionName, '@') + 1);
        $methodParameters = $reflection->getMethod($methodName)->getParameters();

        foreach ($methodParameters as $parameter) {
            if ($parameter->hasType()) {
                $parameterClassName = $parameter->getType()->getName();
                $parameterReflection = new ReflectionClass($parameterClassName);

                if ($parameterReflection->isSubclassOf(FormRequest::class)) {
                    $formRequest = new $parameterClassName();
                    $validationRules = [];

                    try{
                        foreach ($formRequest->rules() as $key => $rule) {
                            $validationRules[] = new LaravelValidationRule($key, $rule);
                        }
                    }catch (\Exception $e){
                        $validationRules[] = new LaravelValidationRule('__ERROR__', 'Rules can not be loaded');
                    }

                    return $validationRules;
                }
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getUri()
    {
        return $this->originalRoute->uri();
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupId()
    {
        return $this->isClosure ? 'closures' : $this->actionClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getSortKey()
    {
        return in_array($this->actionMethod, $this->restMethods) ? $this->actionMethod : $this->getMethods();
    }

    /**
     * {@inheritdoc}
     */
    public function getMethods()
    {
        return $this->originalRoute->methods();
    }

    /**
     * {@inheritdoc}
     */
    public function getValidationRules()
    {
        return $this->validationRules;
    }

    /**
     * {@inheritdoc}
     */
    public function getMiddleware()
    {
        return $this->originalRoute->middleware();
    }
}