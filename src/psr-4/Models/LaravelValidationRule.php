<?php

namespace Snapdesign\Laravel\ApiDoc\Models;


class LaravelValidationRule implements ValidationRule
{
    /**
     * Name of the input
     *
     * @var string
     */
    protected $inputName;

    /**
     * Input rules
     *
     * @var array
     */
    protected $rules = [];

    /**
     * LaravelValidationRule constructor.
     *
     * @param string $inputName
     * @param string $rules
     */
    public function __construct($inputName, $rules)
    {
        $rules = is_array($rules) ? $rules : explode('|', $rules);

        $this->inputName = $inputName;
        $this->rules = array_map(function ($item) {
            if(!is_string($item)){
                dd($item);
            }
            return trim($item);
        }, $rules);
    }

    /**
     * {@inheritdoc}
     */
    public function getInputName()
    {
        return $this->inputName;
    }

    /**
     * {@inheritdoc}
     */
    public function getRules()
    {
        return $this->rules;
    }
}