<?php

namespace Snapdesign\Laravel\ApiDoc\Console\Commands;

use Illuminate\Console\Command;


class ApiDoc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:doc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates Api Documentation based on the routes';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = config('apiDoc');
        $configDefaults = $config['defaults'];
        $configLoaders = $config['loaders'];
        $configGenerators = $config['generators'];
        $configSorters = $config['sorters'];

        $loaderConfig = $configLoaders[$configDefaults['loader']];
        $loader = app()->makeWith($loaderConfig['loader'], ['config' => $loaderConfig]);

        $generatorConfigs = array_map(function ($generatorName) use ($configGenerators) {
            return $configGenerators[$generatorName];
        }, $configDefaults['generators']);

        foreach ($generatorConfigs as $generatorConfig) {
            $generator = app()->makeWith($generatorConfig['generator'], ['config' => $generatorConfig]);

            $sorterName = isset($generatorConfig['sorter']) ? $generatorConfig['sorter'] : $configDefaults['sorter'];
            $sorterConfig = $configSorters[$sorterName];
            $sorter = app()->makeWith($sorterConfig['sorter'], ['config' => $sorterConfig]);

            $routes = $sorter->sort($loader->getRoutes());

            $generator->generate($routes);
        }

        return true;
    }
}
